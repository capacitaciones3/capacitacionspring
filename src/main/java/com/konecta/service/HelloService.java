package com.konecta.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

public class HelloService {

    public List<Object> example() {
        List<Object> list = new ArrayList<>();
        LongStream.range(0, 10).forEach(idx -> list.add("Hi " + idx));
        return list;
    }

}
