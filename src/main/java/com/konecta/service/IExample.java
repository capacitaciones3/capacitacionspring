package com.konecta.service;

import java.util.List;

public interface IExample {

    List<Object> example();

}
