package com.konecta.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.LongStream;

@Service
public class ExampleService implements IExample {

    public List<Object> example() {
        List<Object> list = new ArrayList<>();
        LongStream.range(0, 10).forEach(idx -> list.add("Hi " + idx));
        return list;
    }

}
