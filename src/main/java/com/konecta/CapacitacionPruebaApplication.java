package com.konecta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapacitacionPruebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(CapacitacionPruebaApplication.class, args);
    }

}
