package com.konecta.controller;

import com.konecta.dto.ContactDTO;
import com.konecta.model.Contact;
import com.konecta.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/contacts")
public class ContactRestController {

    @Autowired
    private ContactService contactService;

    @GetMapping
    public List<Contact> getAll() {
        return contactService.findAll();
    }

    @GetMapping("/{id}")
    public ContactDTO findById(@PathVariable("id") Long id) {
        return new ContactDTO();
    }

    @PostMapping("/{id}")
    public boolean create(@PathVariable("id") Long id) {
        return true;
    }

    @PostMapping("/create")
    public Contact create(@RequestBody Contact contact) {
        return contactService.save(contact);
    }

    @PutMapping("/{id}")
    public boolean update(@PathVariable("id") Long id) {
        return true;
    }

    @GetMapping("/search")
    public List<Contact> findByEmail(@RequestParam("email") String email) {
        return contactService.findByEmail(email);
    }
}
