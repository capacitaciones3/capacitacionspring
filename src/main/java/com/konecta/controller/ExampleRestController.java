package com.konecta.controller;

import com.konecta.dto.ErrorDTO;
import com.konecta.exeption.MyExeption;
import com.konecta.service.ExampleService;
import com.konecta.utils.ExampleBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/test")
public class ExampleRestController {

    @Autowired
    private ExampleService exampleService;

    @Autowired
    private ExampleBean exampleBean;

    @GetMapping("/list")
    public List<Object> example() {
        return exampleService.example();
    }

    @GetMapping("/grettings")
    public String grettings(@RequestParam(name = "name", required = false)Long name) {
        return String.format("Hi %s", name);
    }

    @GetMapping("/find")
    public String find(@RequestParam(name = "name", required = false)String name) {
        if("jose".equals(name)) {
            throw new MyExeption(String.format("Name %s no permitted"));
        }
        return String.format("Hi %s", name);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorDTO numberExeptionHandler(Exception e) {
        return new ErrorDTO(400, "Error parsing number", e.getMessage());
    }

    @ExceptionHandler(MyExeption.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorDTO numberExeptionHandler2(Exception e) {
        return new ErrorDTO(400, "Error parsing number", e.getMessage());
    }
}
