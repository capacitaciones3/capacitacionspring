package com.konecta.utils;

import com.konecta.service.IExample;

import java.util.List;

public class ExampleBean {

    private IExample example;
    public ExampleBean(IExample example) {
        this.example = example;
    }

    public List<Object> list() {
        return example.example();
    }

}
