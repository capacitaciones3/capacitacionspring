package com.konecta.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="CONTACT")
public class Contact {

    @Id
    @Column(name="id", updatable = false, nullable = false)
//    @SequenceGenerator(initialValue = 1, name="idGenContact", sequenceName = "contactSEQ")
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "idGenContact")
    private Long id;
    private String firstName;
    private String phone;
    private String email;
    private String address;
    private Date birdDate;

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirdDate() {
        return birdDate;
    }

    public void setBirdDate(Date birdDate) {
        this.birdDate = birdDate;
    }
}
