package com.konecta.config;


import com.konecta.service.IExample;
import com.konecta.utils.ExampleBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AplicationConfig {

    @Bean
    public ExampleBean exampleBean(IExample example) {
        return new ExampleBean(example);
    }

    @Bean
    public ExampleBean exampleBean2(IExample example) {
        return new ExampleBean(example);
    }

}
