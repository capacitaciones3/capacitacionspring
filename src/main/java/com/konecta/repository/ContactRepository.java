package com.konecta.repository;

import com.konecta.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    public List<Contact> findByEmail(String email);
//    public List<Contact> findByEmailAndPhone(String email, String phone);
    @Query("from Contact c where c.email=?1 and c.phone=?2")
    public List<Contact> findByEmailAndPhone(String email, String phone);
}
